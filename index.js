console.log("Hello World");
let firstName = "Juan";
let lastName = "Dela Cruz";
let age = 30;
let hobbies = ["Playing video games", "Reading Books", "Singing"];
let workAddress = {
	houseNumber: "12345",
	street: "Mabini",
	city: "Santiago",
	state: "Isabela"
};
console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);
console.log("Hobbies:");
console.log(hobbies);
console.log("Work Address:");
console.log(workAddress);

let fullName = "Ginber Candelario";
let myAge = 23;
let myFriends = ["Samu", "Warren", "Jonathan", "Kim", "Michael"];
let myFullProfile = {
	userName: "gin_co",
	fullName: "Ginber Candelario",
	age: 23,
	isActive: false
};
let myBestfriend = "Kim Zandro";
let whereIFoundFrozen = "Arctic Ocean";

console.log("My full name is: " + fullName);
console.log("My current age is: " + myAge);
console.log("My friends are:");
console.log(myFriends);
console.log("My Full Profile:");
console.log(myFullProfile);
console.log("My bestfriend is: " + myBestfriend);
console.log("I was found frozen in: " + whereIFoundFrozen);